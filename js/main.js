$(document).ready(function () {
    setTimeout(function () {
        $('.hang-animation').removeClass('slide-in-blurred-left')
    }, 270);
});


jQuery(function($) {
    if ($(window).width() > 769) {
        $('.navbar .dropdown').hover(function() {
            $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

        }, function() {
            $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

        });

        $('.navbar .dropdown > a').click(function() {
            location.href = this.href;
        });

    }
});

